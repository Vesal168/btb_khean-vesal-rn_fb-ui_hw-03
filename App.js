

import React, {Component} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

import FBHeader from './components/FBHeader';
import Icon from 'react-native-vector-icons/FontAwesome';

Icon.loadFont();
 
class App extends Component {
   render() {
     return (
      <View style={styles.container}>
        <FBHeader/>
      </View>
     )
   }
}
const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    flex: 1,
  }
});
 
export default App;
